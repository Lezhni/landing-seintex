$ = jQuery;

$(document).on('ready', ready);
$(document).on('scroll', scroll);
$(window).on('resize', resize);

$('.popup').click(function() {

    var target = $(this).attr('href');
    $.magnificPopup.open({
        items: {
            src: target,
            type: 'inline'
        }
    });

    return false;
});

$('.header-menu a, .scroll-btn').click(function() {

    var block = $(this).attr('href');
    if ($(window).width() > 1000) {
        var header = 50;
    } else {
        var header = 117;
    }

    var path = $(block).offset().top - header;
    $('body, html').animate({
        scrollTop: path
    }, 800);

    return false;
});

$(document).on('click', '.item-color', function() {

    var currColor = $(this).attr('data-color');
    var currColorCode = $(this).attr('data-colorcode');

    $('.item-color').removeClass('item-color-active');
    $(this).addClass('item-color-active');
    $('.item-buy .button-buy').attr('data-color', currColor);

    if (parseInt($('.modal-item .item-gallery-big').attr('data-good')) == 5) {
        $('.modal-item .item-gallery a').each(function() {
            var colorImg = $(this).attr('data-' + currColorCode);
            $(this).attr('href', colorImg);
            $(this).find('img').attr('src', colorImg);
        });
    }
});

$(document).on('change', '.sale-goods-quantity', function() {

    var saleSum = 0;
    var saleQuantity = parseInt($(this).val());

    if (saleQuantity <= 0) {
        saleQuantity = 0;
        $(this).val(0);
        $(this).closest('.sale-goods-single').attr('data-quantity', 0);
    }

    $(this).closest('.sale-goods-single').attr('data-quantity', saleQuantity);

    $('.sale-goods-single').each(function() {
        var item = $(this).attr('data-item-id');
        var quantity = parseInt($(this).attr('data-quantity'));

        saleSum = saleSum + (items[item].price * quantity);
    });

    $('.sale-sum').text('Итого: ' + saleSum + ' Р');
});

$(document).on('click', '.button-sale', function() {

    $('.sale-goods-single').each(function() {

        var item = parseInt($(this).attr('data-item-id'));
        var q = parseInt($(this).attr('data-quantity'));
        if (q !== 0) addToCart(item, 'Черный', q);
    });

    return false;
});

$('.order-form').submit(function() {

    var form = $(this);
    var formData = form.serialize();
    var cartData = sessionStorage.getItem('cart');
    var orderSum = sessionStorage.getItem('totalSum');

    form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');

    if (formData) {
        $.post('order.php', {form: formData, cart: cartData, sum: orderSum}, function(data, textStatus, xhr) {
            if (data == 'sended') {
                $.magnificPopup.open({
                    items: {
                        src: '#thanks',
                        type: 'inline'
                    }
                });
                // #clear cart
                sessionStorage.removeItem('cart');
                $('.header-cart-quantity').text('Корзина пока пуста');
            } else {
                console.log(data);
            }
        });
    }

    form.find('input[type=submit]').removeAttr('disabled').val('Отправить заявку');

    return false;
});

$('.feedback-form').submit(function() {

    var form = $(this);
    var formData = form.serialize();

    form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');

    if (formData) {
        $.post('send.php', formData, function(data, textStatus, xhr) {
            if (data == 'sended') {
                $.magnificPopup.open({
                    items: {
                        src: '#thanks',
                        type: 'inline'
                    }
                });
            } else {
                console.log(data);
            }
        });
    }

    form.find('input[type=submit]').removeAttr('disabled').val('Отправить заявку');

    return false;
});

$('.video-preloader').click(function() {
    var videoID = $(this).attr('data-video');
    var videoIframe = '<iframe width="100%" height="435" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
    $(this).replaceWith(videoIframe);
});

function ready() {

    //sessionStorage.removeItem('cart');

    // #show small cart
    var cart = sessionStorage.getItem('cart');
    var totalQuantity = 0;
    var totalSum = 0;

    if (!cart || cart == '[]') {
        $('.header-cart-quantity').text('Корзина пока пуста');
    } else {
        for (var j = 0; j < cart.length; j++) {
            cart = JSON.parse(cart);
            cartIndex = cart[j];
            totalQuantity = parseInt(totalQuantity) + parseInt(cartIndex.quantity);
            totalSum = parseInt(totalSum) + (parseInt(cartIndex.quantity) * parseInt(items[cartIndex.id].price));
        }

        var cartHTML = totalQuantity + ' товаров на сумму ' + totalSum + ' Р';
        $('.header-cart-quantity').text(cartHTML);
    }

    $('.testimonials-img-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.testimonials-slider'
    });

    $('.testimonials-slider').slick({
        dots: true,
        prevArrow: '<span class="testimonials-slider-prev"></span>',
        nextArrow: '<span class="testimonials-slider-next"></span>',
        asNavFor: '.testimonials-img-slider'
    });

    $('.tooltip').tooltipster({
        contentAsHTML: true,
        position: 'bottom',
        theme: 'tooltipster-default item-tooltip'
    });

    $('[name=feedbackPhone], [name=orderPhone]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});
}

// #scrollspy
$('.animate').on('scrollSpy:enter', function() {

   if (!$(this).hasClass('visible')) {
       $(this).addClass('visible');
   }
});
$('.animate').scrollSpy();


function scroll() {}

function resize() {}

// #cart
// #here list of products
var items = {
    1: {
        name: 'Резиновые коврики Сетка',
        attributes: {
            0: {
                name: 'Артикул',
                value: '83112'
            },
            1: {
                name: 'Материал',
                value: '100% резина'
            },
            2: {
                name: 'Гарантия',
                value: '3 года'
            },
            3: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            }
        },
        price: 2360,
        desc: 'Резиновый коврик Сетка надежно защищает салон автомобиля от внешних факторов. Узор на ковре удерживает в себе грязь и воду. Отличный вариант для сезона осень-зима.'
    },
    2: {
        name: 'Резиновые коврики с высоким бортом',
        attributes: {
            0: {
                name: 'Артикул',
                value: '82222'
            },
            1: {
                name: 'Материал',
                value: '100% резина'
            },
            2: {
                name: 'Гарантия',
                value: '3 года'
            },
            3: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            }
        },
        price: 2310,
        desc: 'Резиновые коврики с высоким бортом защищают салон в любую непогоду. Коврик не пропускает даже большое количество влаги. В первую очередь это достигнуто благодаря увеличенной высоте бортиков - 3,5 сантиметра. От аналогов коврики с высоким бортом отличает цена и использование качественных отечественных материалов. Резина не имеет запаха и безопасна для аллергиков.'
    },
    3: {
        name: 'Ворсовые коврики LUX',
        attributes: {
            0: {
                name: 'Артикул',
                value: '82772'
            },
            1: {
                name: 'Материал',
                value: 'ворс'
            },
            2: {
                name: 'Гарантия',
                value: '1 год'
            },
            3: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            }
        },
        price: 1845,
        desc: 'Ворсовые коврики LUX защитят салон автомобиля в летний сезон. Коврики серии LUX ориентированы на автомобили бизнес класса. Коврики имеют черный цвет и отлично впишутся в салон любой иномарки. При изготовлении используются только качественные материалы отечественного производства.'
    },
    4: {
        name: 'Коврики 3D',
        attributes: {
            0: {
                name: 'Артикул',
                value: '83436'
            },
            1: {
                name: 'Материал',
                value: 'ворс'
            },
            2: {
                name: 'Гарантия',
                value: '1 год'
            },
            3: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            },
            1: {
                name: 'beige',
                value: 'Бежевый'
            },
            2: {
                name: 'grey',
                value: 'Серый'
            }
        },
        price: 3240,
        desc: 'В Seintex производятся автомобильные 3D коврики по современным технологиям. Прочность ковров достигается за счет трехслойной конструкции: ворс, вспененная основа и антискользящая пленка. Материалы проходят тесты качества благодаря чему 3D ковры надежно защищают салон автомобиля.'
    },
    5: {
        name: 'Чехлы в салон',
        attributes: {
            0: {
                name: 'Артикул',
                value: '85356'
            },
            1: {
                name: 'Материал',
                value: 'экокожа'
            },
            // 2: {
            //     name: 'Гарантия',
            //     value: '3 года'
            // },
            2: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            },
            1: {
                name: 'white',
                value: 'Белый'
            },
            2: {
                name: 'grey',
                value: 'Серый'
            }
        },
        price: 4950,
        desc: 'Авточехлы незаменимый аксессуар в машине. Чехлы защищают сиденья автомобиля от повреждений, влаги и мусора. К тому же они скрывают дефекты и преображают внешний вид салона. Авточехлы - отличное дополнение для нового или старого, импортного или отечественного автомобиля.'
    },
    6: {
        name: 'Коврики в багажник (седан)',
        attributes: {
            0: {
                name: 'Артикул',
                value: '82231'
            },
            1: {
                name: 'Материал',
                value: 'полиуретан'
            },
            // 2: {
            //     name: 'Гарантия',
            //     value: '3 года'
            // },
            2: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            }
        },
        price: 1290,
        desc: 'Полимерные коврики в багажник надежно защищают автомобиль от грязи, мусора и повреждений. Коврик экономит время при уборке салона, ведь его легко вытащить из багажника автомобиля и помыть. Ковры в багажник сохраняют внешний вид даже после долгого использования благодаря полимерному материалу (ТЭП). Внешне мягкий и эластичный ТЭП похож на резину.'
    },
    7: {
        name: 'Коврики в багажник (хетчбек)',
        attributes: {
            0: {
                name: 'Артикул',
                value: '82314'
            },
            1: {
                name: 'Материал',
                value: 'полиуретан'
            },
            2: {
                name: 'Гарантия',
                value: '3 года'
            },
            3: {
                name: 'Производитель',
                value: 'Сеинтекс (Россия)'
            }
        },
        color: {
            0: {
                name: 'black',
                value: 'Черный'
            }
        },
        price: 1260,
        desc: 'Полимерные коврики в багажник надежно защищают автомобиль от грязи, мусора и повреждений. Коврик экономит время при уборке салона, ведь его легко вытащить из багажника автомобиля и помыть. Ковры в багажник сохраняют внешний вид даже после долгого использования благодаря полимерному материалу (ТЭП). Внешне мягкий и эластичный ТЭП похож на резину.'
    }
};
// #open item popup
$(document).on('click', '.item-popup', function() {

    var item = parseInt($(this).closest('.goods-single').attr('data-item-id'));
    if (!item) {
        return false;
    }

    var itemName = items[item].name;
    var itemDesc = items[item].desc;
    var itemPrice = items[item].price;

    // #build attributes list
    var i = 0;
    var attrList = '';
    for (var attribute in items[item].attributes) {
        var attrName = items[item].attributes[i].name;
        var attrValue = items[item].attributes[i].value;

        attrList = attrList + '<dl><dt>' + attrName + ':</dt><dd>' + attrValue + '</dd></dl>'
        i++;
    }

    // #build colorpicker
    i = 0;
    var colorsList = '';
    for (var attribute in items[item].color) {
        var colorName = items[item].color[i].name;
        var colorValue = items[item].color[i].value;

        if (i === 0) {
            colorsList = '<span class="item-color item-color-active" data-color="' + colorValue + '" data-colorcode="' + colorName + '"><img src="/img/icon-color-' + colorName + '.png"></span>';
        } else {
            colorsList = colorsList + '<span class="item-color" data-color="' + colorValue + '" data-colorcode="' + colorName + '"><img src="/img/icon-color-' + colorName + '.png"></span>';
        }
        i++;
    }

    // gallery
    var itemImg = '<a href="/img/good-' + item + '.jpg" data-black="/img/good-' + item + '.jpg" data-white="/img/good-' + item + '-white.jpg" data-grey="/img/good-' + item + '-grey.jpg"  data-lightbox="item-image-' + item + '"><img src="/img/good-' + item + '.jpg" alt=""></a>';
    //var itemGall = itemImg + itemImg + itemImg;
    $.magnificPopup.open({
        items: {
            src: '#item',
            type: 'inline'
        },
        callbacks: {
            beforeOpen: function() {
                $('#item .section-title').text(itemName);
                $('#item .item-attributes-list').prepend(attrList);
                $('#item .item-colors-list dd').html(colorsList);
                $('#item .item-desc p').text(itemDesc);
                $('#item .item-buy span').text('Итого: ' + itemPrice + ' Р');
                $('#item .button-buy').attr('data-item-id', item);
                $('#item .item-gallery-big').attr('data-good', item).html(itemImg);
                //$('#item .item-gallery-thmb').html(itemGall);
            },
            close: function() {
                $('#item .item-attributes-list, #item .item-colors-list dd, #item .item-desc p, #item .section-title, #item .item-buy span, #item .item-gallery-big').html('');
                $('#item .button-buy').attr('data-item-id', '');
            }
        }
    });

    return false;
});

// #add to cart
$(document).on('click', '.button-buy', function() {

    var item = parseInt($(this).attr('data-item-id'));
    var color = $(this).attr('data-color');
    addToCart(item, color);

    return false;
});

$(document).on('click', '.button-delete', function() {

    var item = parseInt($(this).attr('data-item-id'));
    var color = $(this).attr('data-color');
    removeFromCart(item, color);
    $(this).closest('tr').remove();

    if (!$('.cart-table tbody tr').length) {
        $('#cart .cart-table, #cart .button-order, #cart .cart-total').hide();
        $('#cart .section-title').text('Корзина пуста');
    }

    return false;
});

$(document).on('click', '.cart-popup', function() {

    // #build cart
    var cart = sessionStorage.getItem('cart');
    if (!cart || cart == '[]') {
        $('#cart .cart-table, #cart .button-order, #cart .cart-total').hide();
        $('#cart .section-title').text('Корзина пуста');
    } else {
        cart = JSON.parse(cart);
        var totalSum = 0;
        var cartTable = '';

        for (var i = 0; i < cart.length; i++) {
            var cartIndex = cart[i];
            cartTable = cartTable + '<tr><td>' + items[cartIndex.id].name + '</td><td>' + items[cartIndex.id].price + '</td><td>' + cartIndex.quantity + '</td><td>' + cartIndex.color + '</td><td><a href="#" class="button button-outline-red button-delete" data-item-id="' + cartIndex.id + '">Убрать из корзины</a></td></tr>';
            totalSum = totalSum + (parseInt(cartIndex.quantity) * parseInt(items[cartIndex.id].price));
        }
    }

    $.magnificPopup.open({
        items: {
            src: '#cart',
            type: 'inline'
        },
        callbacks: {
            beforeOpen: function() {
                $('#cart .cart-table tbody').html(cartTable);
                $('#cart .cart-total').text('Итого: ' + totalSum + ' Р');
            },
            close: function() {
                $('#cart .section-title').text('Корзина');
                $('#cart .cart-table, #cart .button-order, #cart .cart-total').show();
                $('#cart .cart-table tbody').html('');
                $('#cart .cart-total').text('');
            }
        }
    });

    return false;
});

function addToCart(item, color, quantity) {

    item = parseInt(item);

    if (!item) {
        return false;
    }
    if (!color) {
        var color = 'Черный';
    }

    if (!quantity) {
        quantity = 1;
    }

    var newItem = {
        'id': item,
        'name': items[item].name,
        'color': color,
        'quantity': quantity
    };

    // #get cart data or initialize
    var cart = sessionStorage.getItem('cart');
    if (!cart) {
        var cart = [];
    } else {
        cart = JSON.parse(cart);
    }

    totalSum = 0;
    totalQuantity = 0;

    // #change quantity or add item to cart
    if (cart.length !== 0) {
        var p = 0;
        for (var j = 0; j < cart.length; j++) {
            var cartIndex = cart[j];
            if (cartIndex.id === newItem.id && cartIndex.color === newItem.color) {
                cartIndex.quantity = cartIndex.quantity + newItem.quantity;
                p = 1;
                break;
            }
        }
        if (p !== 1) {
            cart.push(newItem);
        }
    } else {
        cart.push(newItem);
    }

    for (j = 0; j < cart.length; j++) {
        var cartIndex = cart[j];
        totalQuantity = parseInt(totalQuantity) + parseInt(cartIndex.quantity);
        totalSum = parseInt(totalSum) + (parseInt(cartIndex.quantity) * parseInt(items[cartIndex.id].price));
    }

    var cartHTML = totalQuantity + ' товаров на сумму ' + totalSum + ' Р';
    $('.header-cart-quantity').text(cartHTML);

    var n = noty({
        text: 'Единица товара "' + newItem.name + '" добавлена в корзину',
        layout: 'bottomRight',
        type: 'success',
        animation: {
            open: {height: 'show'}
        },
        timeout: 3000
    });

    // #send event to roistat
    roistat.event.send('add_cart');

    // #write cart to sessionStorage
    sessionStorage.setItem('cart', JSON.stringify(cart));
    sessionStorage.setItem('totalQuantity', totalQuantity);
    sessionStorage.setItem('totalSum', totalSum);

    return false;
}

function removeFromCart(item, color) {

    item = parseInt(item);

    if (!item) {
        return false;
    }
    if (!color) {
        var color = 'Черный';
    }

    var removeItem = {
        'id': item,
        'name': items[item].name,
        'color': color
    };

    // #get cart data or initialize
    var cart = sessionStorage.getItem('cart');
    if (!cart) {
        var cart = [];
    } else {
        cart = JSON.parse(cart);
    }

    totalSum = 0;
    totalQuantity = 0;

    // #find item and remove
    if (cart.length !== 0) {
        for (var j = 0; j < cart.length; j++) {
            var cartIndex = cart[j];
            if (cartIndex.id === removeItem.id && cartIndex.color === removeItem.color) {
                cart.splice(cartIndex, 1);
                break;
            }
        }
    }

    for (j = 0; j < cart.length; j++) {
        var cartIndex = cart[j];
        totalQuantity = parseInt(totalQuantity) + parseInt(cartIndex.quantity);
        totalSum = parseInt(totalSum) + (parseInt(cartIndex.quantity) * parseInt(items[cartIndex.id].price));
    }

    if (totalSum === 0) {
        $('.header-cart-quantity').text('Корзина пока пуста');
    } else {
        var cartHTML = totalQuantity + ' товаров на сумму ' + totalSum + ' Р';
        $('.header-cart-quantity').text(cartHTML);
    }

    $('#cart .cart-total').text('Итого: ' + totalSum + ' Р');

    var n = noty({
        text: 'Товар "' + removeItem.name + '" удален из корзины',
        layout: 'bottomRight',
        type: 'success',
        animation: {
            open: {height: 'show'}
        },
        timeout: 3000
    });

    // #write cart to sessionStorage
    sessionStorage.setItem('cart', JSON.stringify(cart));
    sessionStorage.setItem('totalQuantity', totalQuantity);
    sessionStorage.setItem('totalSum', totalSum);

    return false;
}
