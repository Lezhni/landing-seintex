<?php

if ($_POST) {

    $orderData = array();
    $orderSum = (int)$_POST['sum'];
    parse_str($_POST['form'], $orderData);

    if ($_POST['cart']) {
        $cart = json_decode($_POST['cart'], true);
        $order = 'Содержимое заказа:';
        foreach ($cart as $cartItem) {
            $order .= " {$cartItem['name']} {$cartItem['color']} {$cartItem['quantity']} шт.;";
        }
    }

    $roistatData = array(
        'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
        'key'     => 'MTIwOTE6MTQ2OTI6NTEzYWQyNDE0ZDU1OWY1OTg0ZTg3ZjdkMDk5ODkxOGQ=',
        'title'   => 'Заказ с лендинга',
        'comment' => $order,
        'name'    => $orderData['orderContact'],
        'phone'   => $orderData['orderPhone'],
        'is_need_callback' => '0',
        'fields' => array(
            'price' => $orderSum
        )
    );

    $answer = file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
    $answer = json_decode($answer, true);
    if ($answer['status'] == 'success') {
        die('sended');
    } else {
        var_dump($answer);
    }
}

die();
